@isTest
public class TestDataFactory2 {
    public static List<ID> createAccountsWithContacts(Integer numAccts, Integer numContactsPerAcct) {
        List<Account> accts = new List<Account>();
        List<ID> acctIds = new List<ID>();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            accts.add(a);
        }
        insert accts;
        
        for (Account a: accts) {
            acctIds.add(a.Id);
        }            
           
               
        List<Contact> conts = new List<Contact>();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];
            // For each account just inserted, add contacts
            for (Integer k=0;k<numContactsPerAcct;k++) {
                conts.add(new Contact(firstName=acct.Name,
                                      lastName= ' Contact ' + k,
                                      AccountId=acct.Id));
            }
        }
        // Insert all contatct for all accounts.
        insert conts;
        System.debug(acctIds);
        return acctIds;
    }
}