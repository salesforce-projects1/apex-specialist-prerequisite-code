@isTest
private class TestVerifyDate {
    @isTest static void testCheckDatesWithin30Days() {
        Date date1 = date.newinstance(2021, 2, 17);
        Date date2 = date1.addDays(10);
        Date d = VerifyDate.CheckDates(date1, date2);
        System.assertEquals(d, date2);
    }
    @isTest static void testCheckDatesMoreThan30Days() {
        Date date1 = date.newinstance(2021, 2, 17);
        Date date2 = date1.addDays(31);
        Date d = VerifyDate.CheckDates(date1, date2);
        System.assertEquals(d, date.newinstance(2021, 2, 28));
    }
}