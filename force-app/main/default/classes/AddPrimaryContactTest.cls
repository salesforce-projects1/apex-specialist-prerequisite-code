@isTest
public class AddPrimaryContactTest {
    @testSetup
    static void setup() {
        List<Account> accounts = new List<Account>();
        
        for (Integer i = 0; i < 50; i++) {
            accounts.add(new Account(
                Name='Test Account NY'+i,
                BillingState='NY'
            ));
            
        }
        insert accounts;
    }
    static testmethod void testQueueable() {
        String state = 'NY';
		Contact contact = new Contact(
			lastName='NY',
			firstName='John');
		// instantiate a new instance of the Queueable class
		AddPrimaryContact apc = new AddPrimaryContact(contact, state);
		
        // startTest/stopTest block to force async processes to run
        Test.startTest();
        System.enqueueJob(apc);
        Test.stopTest();
        // Validate the job ran. Check if record have correct parentId now
        System.assertEquals(50, [select count() from contact where lastName = 'NY']);
    }
}