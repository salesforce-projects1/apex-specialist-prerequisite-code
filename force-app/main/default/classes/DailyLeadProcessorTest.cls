@isTest
private class DailyLeadProcessorTest {
    // Dummy CRON expression: midnight on March 15.
    // Because this is a test, job executes
    // immediately after Test.stopTest().
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void testScheduledJob() {
        
        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<200; i++) {
            Lead l = new Lead(
                lastName = 'Lead ' + i,
                Company = 'Hammertoe',
                Status = 'Open - Not Contacted'
            );
            leads.add(l);
        }
        insert leads;
        
        // Get the IDs of the opportunities we just inserted
        Map<Id, Lead> leadMap = new Map<Id, Lead>(leads);
        List<Id> leadIds = new List<Id>(leadMap.keySet());
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP,
            new DailyLeadProcessor());
        // Verify the scheduled job has not run yet.
        List<Lead> lt = [SELECT Id
            FROM Lead
            WHERE Company = 'Hammertoe'
            AND leadSource = ''];
        System.assertEquals(200, lt.size(), 'Leads have empty source');
        // Stopping the test will run the job synchronously
        Test.stopTest();
        // Now that the scheduled job has executed,
        // check that our tasks were created
        lt = [SELECT Id
            FROM Lead
            WHERE Company = 'Hammertoe'
            AND leadSource = ''];
        System.assertEquals(0,
            lt.size(),
            'Tasks were not created');
    }
}