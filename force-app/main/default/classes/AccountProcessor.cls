global class AccountProcessor {
  @future
  //  This method counts the number of Contact records associated to each Account ID passed to the method
  //  and updates the 'Number_Of_Contacts__c' field with this value
  public static void countContacts(List<Id> accountIds) {
    List<Account> accounts = [Select Id from Account Where Id IN :accountIds];
      for (Account a: accounts) {
          //Integer count = [SELECT COUNT() FROM CONTACT WHERE AccountId = :a.Id];
          a.Number_Of_Contacts__c = [SELECT COUNT() FROM CONTACT WHERE AccountId = :a.Id];
      }
      update accounts;
  }
}