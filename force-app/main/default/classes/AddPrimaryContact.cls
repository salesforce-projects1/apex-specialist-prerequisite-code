public class AddPrimaryContact implements Queueable{
	private List<Account> accounts;
    private Contact contact;
    private String state;
    public AddPrimaryContact(Contact con, String st) {
        this.contact = con;
        this.state = st;
    }
    public void execute(QueueableContext context) {
        accounts =  [Select Name From account where billingState = :state];
        for (Account account : accounts) {
                Contact con = (new Contact(
                	lastName=contact.lastName,
                	firstName=contact.firstName,
                    AccountId=account.Id
                ));
                insert con;
            
         
    	}
      }
        
}