@IsTest
private class AccountProcessorTest {
  @IsTest
  private static void testCountContacts() {
      List<ID> mockAccountIds = TestDataFactory2.createAccountsWithContacts(5,2);
      System.debug(mockAccountIds);
    Test.startTest();
      AccountProcessor.countContacts(mockAccountIds);
    Test.stopTest();
    // runs callout and check results
    ID id1 = mockAccountIds[0];
      System.debug(id1);
    Decimal numOfContacts = [Select Name From Account Where Id = :id1 LIMIT 1].Number_Of_Contacts__c;
    System.assertEquals(numOfContacts, 2);
        
    
    // System.assertEquals('success', logs[0].msg__c);
  }
}