public class ParkLocator {
    public static String[] country(String country) {
        ParkService.ParksImplPort getParks = 
            new ParkService.ParksImplPort();
        return getParks.byCountry(country);
    }
}