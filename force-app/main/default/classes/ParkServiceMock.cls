@isTest
global class ParkServiceMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        // start - specify the response you want to send
        ParkService.byCountryResponse response_x = 
            new ParkService.byCountryResponse();
        String[] actual = new List<String>();
        actual.add('Shiretoko National Park');
        actual.add('Oze National Park');
        actual.add('Hakusan National Park');
        response_x.return_x = actual;
        // end
        response.put('response_x', response_x); 
   }
}