@isTest
private class ParkLocatorTest {
	@isTest static void testCallout() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        // Call the method that invokes a callout
        String country = 'Japan';
        String[] result = ParkLocator.country(country);
        String[] actual = new List<String>();
        actual.add('Shiretoko National Park');
        actual.add('Oze National Park');
        actual.add('Hakusan National Park');
        // Verify that a fake result is returned
        System.assertEquals(actual , result); 
    }
}
//(Shiretoko National Park, Oze National Park, Hakusan National Park)