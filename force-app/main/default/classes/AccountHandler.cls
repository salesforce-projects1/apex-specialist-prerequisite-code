public class AccountHandler {
    public static Account insertNewAccount(String accountName) {
        try {
            Account account = new Account(Name = accountName);
            insert account;
            return account;
        } catch (DmlException e) {
            System.debug('A DML exception has occurred: ' +
                e.getMessage());
            return null;
        }
    }
}