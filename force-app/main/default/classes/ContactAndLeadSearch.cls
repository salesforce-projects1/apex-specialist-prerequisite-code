public class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String matcher) {
        List<List<sObject>> searchList = [FIND :matcher IN NAME FIELDS 
                   RETURNING Lead,Contact];
        return searchList;
    }
}