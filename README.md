# Apex Specialist Superbadge Prerequisite Code

### This repo contains all files used in the Apex Developer Superbadge Prerequisite modules.

[Apex Triggers](https://trailhead.salesforce.com/en/content/learn/modules/apex_triggers)

[Apex Testing](https://trailhead.salesforce.com/en/content/learn/modules/apex_testing)

[Asynchronous Apex](https://trailhead.salesforce.com/en/content/learn/modules/asynchronous_apex)

[Apex Integration Services](https://trailhead.salesforce.com/en/content/learn/modules/apex_integration_services)


All Apex classes and tests can be found under 
 [force-app\main\default\classes](https://gitlab.com/salesforce-projects1/apex-specialist-prerequisite-code/-/tree/main/force-app/main/default/classes)

All Apex triggers are found under
 [force-app\main\default\triggers](https://gitlab.com/salesforce-projects1/apex-specialist-prerequisite-code/-/tree/main/force-app/main/default/triggers)


